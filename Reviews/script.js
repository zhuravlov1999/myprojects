"use strict";
const reviews = [
  {
    personName: "Nicky Wallson",
    photo: "/person-1.png",
    job: "Web-developer",
    review: "Woody equal ask saw sir weeks aware decay. Entrance prospect removing we packages strictly is no smallest he. For hopes may chief get hours day rooms. Oh no turned behind polite piqued enough at. Forbade few through inquiry blushes you.",
  },
  {
    personName: "Sarah Sanches",
    photo: "/person-2.png",
    job: "UI/UX designer",
    review: "He went such dare good mr fact. The small own seven saved man age ﻿no offer. Suspicion did mrs nor furniture smallness. Scale whole downs often leave not eat.",
  },
  {
    personName: "Maria Gonsales",
    photo: "/person-3.png",
    job: "Progect manager",
    review: "In up so discovery my middleton eagerness dejection explained. Estimating excellence ye contrasted insensible as. Oh up unsatiable advantages decisively as at interested. Present suppose in esteems in demesne colonel it to.",
  },
  {
    personName: "Bart Brogovich",
    photo: "/person-4.png",
    job: "QA",
    review: "Am no an listening depending up believing. Enough around remove to barton agreed regret in or it. Advantage mr estimable be commanded provision. Year well shot deny shew come now had. Shall downs stand marry taken his for out. ",
  },
];

//Selecting DOM

const photoEl = document.querySelector(".photo");
const jobEl = document.querySelector(".job");
const personNameEl = document.querySelector(".personName");
const textEl = document.querySelector(".text");

const nextBtn = document.querySelector(".next");
const prevBtn = document.querySelector(".prev");
const randomBtn = document.querySelector(".random");
const btns = document.querySelectorAll(".btn");

//Starting conditions

let currentIndex = 0;

const person = function () {
  const index = reviews[currentIndex];
  photoEl.src = index.photo;
  jobEl.textContent = index.job;
  personNameEl.textContent = index.personName;
  textEl.textContent = index.review;
};

btns.forEach(function (item) {
  item.addEventListener("click", function (e) {
    const event = e.currentTarget.classList;
    if (event.contains("next")) {
      currentIndex++;
      if (currentIndex > reviews.length - 1) {
        currentIndex = 0;
      }
      person();
    } else if (event.contains("prev")) {
      currentIndex--;
      if (currentIndex < 0) {
        currentIndex = reviews.length - 1;
      }
      person();
    } else if (event.contains("random")) {
      const randomNumber = Math.trunc(Math.random() * reviews.length - 1);
      const random = reviews[randomNumber];
      photoEl.src = random.photo;
      jobEl.textContent = random.job;
      personNameEl.textContent = random.personName;
      textEl.textContent = random.review;
    }
  });
});
