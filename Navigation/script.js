'use strict'

const mainHead = document.querySelector('.main-head')
const burgerBtn = document.querySelector('.burger')
const navbar = document.querySelector('.navbar')

burgerBtn.addEventListener('click', function(){
    mainHead.classList.toggle('show-nav')
    navbar.classList.toggle('hidden')
})