"use strict";

// Selecting

const value = document.getElementById("value");
const btns = document.querySelectorAll(".btn");

//Starting conditions

let currentValue = 0;

// Buttons. Decrease button

btns.forEach(function (btn) {
  btn.addEventListener("click", function (e) {
    const styles = e.currentTarget.classList;
    console.log(styles);
    if (styles.contains("decrease")) {
      currentValue--;
    } else if (styles.contains("increase")) {
      currentValue++;
    } else if (styles.contains("reset")) {
      currentValue = 0;
    }
    value.textContent = currentValue;
    if (currentValue > 0) {
      value.style.color = "green";
    } else if (currentValue < 0) {
      value.style.color = "red";
    } else {
      value.style.color = "#202020";
    }
  });
});
