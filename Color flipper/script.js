"use strict";
const hex = [1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];
const btn = document.getElementById("btn");
const hexResult = document.querySelector(".hex-result");

btn.addEventListener("click", function () {
  let hexNumber = "#";
  for (let i = 0; i < 6; i++) {
    const randomNumber = Math.trunc(Math.random() * hex.length);
    hexNumber += hex[randomNumber];
  }
  document.body.style.backgroundColor = hexNumber;
  hexResult.textContent = hexNumber;
});
